<?php

class Products extends \Phalcon\Mvc\Model
{

   /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $typesId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $price;

    /**
     * @var integer
     */
    public $quantity;

    /**
     * @var string
     */
    public $status;

    /**
     * Este método establece el valor del campo id
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Este método establece el valor del campo typesId
     *
     * @param integer $typesId
     */
    public function setTypesId($typesId)
    {
        $this->typesId = $typesId;
    }

    // ...

    /**
     * Returna el valor del campo status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

}
